package automatization;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {

    public static WebDriver driver;
    public static File scrFile;
    public static int screenNumber = 1;

    public static void main(String[] args) throws IOException {
	    // Go to website
        String screenShotPath = System.getProperty("user.dir") + File.separator + "Screenshots";
        System.out.println("screenShotPath: " + screenShotPath);
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\Student\\IdeaProjects\\testowanie-2\\jars\\chromedriver.exe");
        driver = new ChromeDriver();
        System.out.println("Launching Chrome...");
        driver.manage().window().maximize();
        driver.get("https://www.wse.edu.pl/");

        // Close the popup
        waitandClick("#rolnictwo > span");
        System.out.println("Closed the popup...");
        Screenshot(screenShotPath);

        // Go to 'Dla kandydata'
        waitandClick("#header > div > div.menu > nav > ul > li:nth-child(3) > a");
        System.out.println("Clicked on 'Dla kandydata'...");
        Screenshot(screenShotPath);

        // Close cookies alert
        waitandClick("#cookies > div > div");
        System.out.println("Closed cookies alert...");

        // Go to 'Studia podyplomowe'
        waitandClick("#main > section.bg-square.subbanner > div > div.content.vpads-s > ul > li:nth-child(2) > a");
        System.out.println("Clicked on 'Studia podyplomowe'...");
        Screenshot(screenShotPath);

        // Scroll to 'Zadzwoń'
        javascriptScroll(By.xpath("//*[@id=\"main\"]/section[3]/div/div/div/div[1]/div[1]"));
        System.out.println("Scrolled to 'Zadzwoń'...");

        // Click on telephone number
        waitandClick("#main > section:nth-child(3) > div > div > div > div:nth-child(1) > div:nth-child(3) > span");
        System.out.println("Click on telephone number...");

        var number = driver.findElement(By.cssSelector("#main > section:nth-child(3) > div > div > div > div:nth-child(1) > div:nth-child(3) > span")).getText();
        System.out.println("Telephone number: " + number);

        // Go to 'Dla pracownika'
        javascriptScroll(By.xpath("//*[@id=\"header\"]/div/div[1]/nav/ul/li[4]/a"));
        waitandClick("#header > div > div.menu > nav > ul > li:nth-child(4) > a");
        System.out.println("Clicked on 'Dla pracownika'...");
        Screenshot(screenShotPath);

        // Find all blog posts
        var posts = driver.findElements(By.className("site-el"));
        var postsLength = posts.stream().count();

        if (postsLength == 6) {
            System.out.println("There are 6 posts on the website...");
        } else {
            System.out.println("Number of posts: " + postsLength);
        }

        // Scroll to 'Niepełnosprawni'
        javascriptScroll(By.xpath("//*[@id=\"footer\"]/div/div[2]/ul/li[14]/a"));
        System.out.println("Scrolled to 'Niepełnosprawni'...");

        // Go to 'Niepełnosprawni'
        waitandClickBy(By.xpath("//*[@id=\"footer\"]/div/div[2]/ul/li[14]/a"));
        System.out.println("Clicked on 'Niepełnosprawni'...");
        Screenshot(screenShotPath);

        // Scroll to 'Wyszukaj'
        javascriptScroll(By.xpath("//*[@id=\"main\"]/section[4]/div/div/div/div[5]/div[1]"));
        System.out.println("Scrolled to 'Wyszukaj'...");

        // Search for 'projekty'
        driver.findElement(By.id("searchInput")).sendKeys("projekty");
        waitandClickBy(By.id("search_button"));
        System.out.println("Search for 'projekty'...");

        // Check search results
        var searchResults = driver.findElements(By.className("bl-head"));
        var searchResultsLength = searchResults.stream().count();

        var allSearchResults = driver.findElement(By.cssSelector("#main > section:nth-child(3) > div > div.c-head > span:nth-child(1)")).getText();

        if (allSearchResults.isEmpty()) {
            System.out.println("Nothing was found...");
        } else {
            System.out.println("All search results: " + allSearchResults);
            System.out.println("Number of search items on first page: " + searchResultsLength);
        }

        // TODO: Check number of last page
        javascriptScroll(By.xpath("//*[@id=\"main\"]/section[4]/div/div[2]"));
        var lastPage = driver.findElement(By.cssSelector("#main > section:nth-child(4) > div > div.page-list > ul > li:last-child"));
        var lastPageClass = lastPage.getAttribute("class").equals("act");

//        while (lastPageClass ==! true) {
//            javascriptScroll(By.xpath("//*[@id=\"main\"]/section[4]/div/div[2]"));
//            driver.findElement(By.cssSelector("#main > section:nth-child(4) > div > div.page-list > ul > li:last-child")).click();
//            lastPageClass = lastPage.getAttribute("class").equals("act");
//        }

//        var lastPageNumber = driver.findElement(By.xpath("//*[@id=\"main\"]/section[4]/div/div[2]/ul/li[5]")).getText();
//        System.out.println("Number of last page: " + lastPageNumber);

    }

    public static void waitandClick(String css)
    {
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
        driver.findElement(By.cssSelector(css)).click();
    }

    public static void waitandClickBy(By by)
    {
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        wait.until(ExpectedConditions.elementToBeClickable(by));
        driver.findElement(by).click();
    }

    public static void Screenshot(String screenShotPath) throws IOException
    {
        scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(screenShotPath + File.separator + "Image" + screenNumber + ".png"));
        System.out.println("Screenshotted: " + new File(screenShotPath + File.separator + "Image" + screenNumber + ".png"));
        screenNumber = screenNumber + 1;
    }

    public static void javascriptScroll(By by) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement element = driver.findElement(by);
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
